import nltk.classify.util
from nltk.classify import NaiveBayesClassifier
import string
import random


def GetWords(sentence):
    punc = set(string.punctuation)
    for p in punc:
        sentence = sentence.replace(p, " ")
    words = sentence.split(' ')
    result = []
    for word in words:
        word = word.strip().lower()
        if word != "":
            result.append(word)
    return result


f_pos = 'data/out_quotes.txt'
f_neg = 'data/fml_negative.txt'

f_pos = open(f_pos).readlines()
f_neg = open(f_neg).readlines()

for i in range(len(f_pos)):
    f_pos[i] = GetWords(f_pos[i])
    temp = dict([(word, True) for word in f_pos[i]])
    f_pos[i] = (temp, 'pos')

for i in range(len(f_neg)):
    f_neg[i] = GetWords(f_neg[i])
    temp = dict([(word, True) for word in f_neg[i]])
    f_neg[i] = (temp, 'neg')

#80-20 testing.  We have more positive examples than negative examples
poscutoff = negcutoff = int(len(f_pos)*.8)
f_neg = f_neg[:poscutoff]

random.shuffle(f_pos)
random.shuffle(f_neg)

trainfeats = f_neg[:negcutoff] + f_pos[:poscutoff]
testfeats = f_neg[negcutoff:] + f_pos[poscutoff:]
print 'train on %d instances, test on %d instances' % (len(trainfeats), len(testfeats))

classifier = NaiveBayesClassifier.train(trainfeats)
print 'accuracy:', nltk.classify.util.accuracy(classifier, testfeats)
classifier.show_most_informative_features()

