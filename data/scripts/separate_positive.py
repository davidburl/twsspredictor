import re

file_in = 'out.txt'
file_out = 'out_quotes.txt'

data = []
f_in = open(file_in)
f_out = open(file_out, 'w+')
text = f_in.readlines()

for i in range(0,len(text)):
    line = text[i]
    matches = re.findall(r'"(.*?)"', line)
    try:
        temp = matches.pop()
        data.append(matches.pop())
    except IndexError:
        continue


for entry in data:
    f_out.write(entry)
    f_out.write('\n')

