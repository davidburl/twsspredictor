file_in = "fml.txt"
file_out = 'fml_negative.txt'

data = []
f_in = open(file_in)
f_out = open(file_out, 'w+')

text= f_in.readlines()

for i in range(0, len(text)):
    line = text[i]
    line = line.split('.')
    if len(line) > 1:
        data.append(line.pop(0))

for entry in data:
    f_out.write(entry)
    f_out.write('\n')
