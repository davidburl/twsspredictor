import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.net.SocketTimeoutException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class FMLParser {

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		BufferedWriter out = new BufferedWriter(new FileWriter("out.txt"));
		//int numPages = Integer.parseInt(args[0]);		
		int numPages = 228;
		for(int i=0;i<numPages;i++) {
			Document doc = null;
			int timeout=0;
			while(doc == null) {
				try {
					doc = Jsoup.connect("http://twssstories.com/node?page="+i).get();
				} catch (SocketTimeoutException e) {
					timeout++;
					if(timeout == 5) {
						System.out.println("5x timeouts");
						return;
					}
				}
			}
			
			Elements twssComment = doc.getElementsByAttributeValue("class","content clear-block");
			for(int j=0;j<twssComment.size();j++) {
				Elements comments = twssComment.get(j).getElementsByTag("p");
				if(comments.size() > 0) {
					String finalTWSS = comments.get(0).text().replace("TWSS","").replace("TWWSS","");
					out.write(finalTWSS+"\n");
					System.out.println(finalTWSS);
				}
			}
		}
		out.close();
	}
}
