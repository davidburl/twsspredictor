import math
import string
import operator

class Predictor():
    
    def __init__(self, positive, negative, bivalues=False):
    #Constructor accepts a list of positive and negative data"""
        # create a dictionary object. ex: cat['pos'] = [set(), set() ....]
        self.category = {}
        self.category['pos'] = []
        self.category['neg'] = []
        self.bivalues = bivalues
		
        for sentence in positive:
            if bivalues:
                sentence = GetWords(sentence)
		temp = []
                for i in range(len(sentence)-1):
                    cat = sentence[i] + ' ' + sentence[i+1]
                    temp.append(cat)
                temp = set(temp)
            else:
                temp = set(GetWords(sentence))
            self.category['pos'].append(temp) 

        for sentence in negative:
            if bivalues:
                sentence = GetWords(sentence)
		temp = []
                for i in range(len(sentence)-1):
                    cat = sentence[i] + ' ' + sentence[i+1]
                    temp.append(cat)
                temp = set(temp)
            else:
	        temp = set(GetWords(sentence))
            self.category['neg'].append(temp)
    
    def getProbabilities(self):
        positive_set = [] 
        negitive_set = []
        return_set_pos = {}
        return_set_neg = {}
        for doc in self.category['pos']:
            for word in doc:
                positive_set.append(word)
        for doc in self.category['neg']:
            for word in doc:
                negitive_set.append(word)
        positive_set = set(positive_set)
        negitive_set = set(negitive_set)
        for word in positive_set:
            positive_value = math.log((self.Dci(word, 'pos') + 1) / (self.Di(word) + 2))
            return_set_pos[word] = positive_value
        for word in negitive_set:
            negitive_value = math.log((self.Dci(word, 'neg') + 1) / (self.Di(word) + 2))
            return_set_neg[word] = negitive_value
        return_set_pos = sorted(return_set_pos.iteritems(),key=operator.itemgetter(1),reverse=False)
        return_set_neg = sorted(return_set_neg.iteritems(),key=operator.itemgetter(1),reverse=False)
        return (return_set_neg, return_set_pos)

    #remove uncommon words
    def removeUncommonWords(self, minNumber):
        numRemoved = 0
        counts = {}
        # count the amount of words
        for doc in self.category['pos']:
            for word in doc:
                counts.setdefault(word,0)
                counts[word] += 1
        for doc in self.category['neg']:
            for word in doc:
                counts.setdefault(word,0)
                counts[word] += 1

        # remove the words if they are less than minNum
        for doc in self.category['pos']:
            wordsToDelete = []
            for word in doc:
                if counts[word] < minNumber:
                    wordsToDelete.append(word)
            for word in wordsToDelete:
                numRemoved = numRemoved + 1
                doc.remove(word)
        for doc in self.category['neg']:
            wordsToDelete = []
            for word in doc:
                if counts[word] < minNumber:
                    wordsToDelete.append(word)
            for word in wordsToDelete:
                numRemoved = numRemoved + 1
                doc.remove(word)
        return numRemoved


    def Pci(self, sentence):
    #List representing the words in a sentence. Returns a tuple of (positive,negative)"""
        positive = 0
        negative = 0

	# adding support for bivalues
        if self.bivalues:
            temp = []
            for i in range(len(sentence)-1):
                cat = sentence[i]+' ' + sentence[i+1]
                temp.append(cat)
            sentence = temp
        #loop for positive
        for word in sentence:
            positive += math.log((self.Dci(word, 'pos') + 1) / (self.Di(word) + 2))
            negative += math.log((self.Dci(word, 'neg') + 1) / (self.Di(word) + 2))
        positive += self.Pcc('pos')
        negative += self.Pcc('neg')
        return (positive, negative)

    def Pcc(self, cat):
    #Pcc step"""
        nc = len(self.category[cat]) + 1
        keys = self.category.keys()
        n = 0
        for key in keys:
            n += len(self.category[key])
        return math.log(float(nc) / (n + len(keys)) )
    
    def Di(self, word):
    #get the di value"""
        di = 0
        keys = self.category.keys()
        for cat in keys:
            for doc in self.category[cat]:
                if word in doc:
                    di += 1
        return float(di)

    def Dci(self, word, cat):
    #get the dci value"""
        dci = 0
        for doc in self.category[cat]:
            if word in doc:
                dci+= 1
        return float(dci)

    def test(self, phrase):
        tested = GetWords(phrase)
        x = self.Pci(tested)
        return x

# making sure that the sentences are properly split and formatted
def GetWords(sentence):
    punc = set(string.punctuation)
    punc.remove("'")
    for p in punc:
        sentence = sentence.replace(p, " ")
    words = sentence.split(' ')
    result = []
    for word in words:
        word = word.strip().lower()
        if word != "":
            result.append(word)
    return result
