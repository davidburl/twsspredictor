import predictor
import random

f_pos = 'data/out_quotes.txt'
f_neg = 'data/fml_negative.txt'

f_pos = open(f_pos).readlines()
f_neg = open(f_neg).readlines()

for i in range(len(f_pos)):\
    f_pos[i] = f_pos[i].strip()

for i in range(len(f_neg)):\
    f_neg[i] = f_neg[i].strip()



n = .8 * len(f_pos)

random.shuffle(f_pos)
random.shuffle(f_neg)

positive_train = f_pos[:int(n)]
positive_test = f_pos[int(n):]

negative_train = f_neg[:len(positive_train)]
negative_test = f_neg[len(positive_train):len(positive_test)+len(positive_train)]

p = predictor.Predictor(positive_train, negative_train, bivalues=False)

print p.removeUncommonWords(2)

#probabilities
probabities = p.getProbabilities()
negFile = open("negative_words.txt", "w+")
for x in probabities[0]:
    negFile.write("(")
    negFile.write(x[0])
    negFile.write(", ")
    negFile.write(str(x[1]))
    negFile.write(")")
    negFile.write("\r\n")
negFile.close()

posFile = open("positive_words.txt", "w+")
for x in probabities[1]:
    posFile.write("(")
    posFile.write(x[0])
    posFile.write(", ")
    posFile.write(str(x[1]))
    posFile.write(")")
    posFile.write("\r\n")
posFile.close()
# end probabilities

tn = 0 #case negative, found negative
fn = 0 #case positive, found negative
tp = 0 #case positive, found positive
fp = 0 #case negative, found positive

for i in range(len(positive_test)):
    x = p.test(positive_test[i])
    pos = x[0]
    neg = x[1]
    if pos > neg:
        tp += 1
    else:
        fn += 1

for i in range(len(negative_test)):
    x = p.test(negative_test[i])
    pos = x[0]
    neg = x[1]
    if pos < neg:
        tn += 1
    else:
        fp += 1

tp = float(tp)
accuracy = (tp + tn) / (len(positive_test)+len(negative_test))
recall = tp/(tp+fn)
precision = tp / (tp + fp)
fscore = 2.0*precision*recall/(precision+recall)

print "Accuracy: " + str(accuracy)
print "Recall: " + str(recall)
print "Precision: " + str(precision)
print "F-Score: " + str(fscore)
