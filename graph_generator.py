import predictor
import random

f_pos = 'data/out_quotes.txt'
f_neg = 'data/fml_negative.txt'

f_pos = open(f_pos).readlines()
f_neg = open(f_neg).readlines()

for i in range(len(f_pos)):\
    f_pos[i] = f_pos[i].strip()

for i in range(len(f_neg)):\
    f_neg[i] = f_neg[i].strip()



n = .8 * len(f_pos)

# Change this to alter step
step = range(10,int(n),25)
print len(step)

accuracy_list = [0]*len(step)
precision_list = [0]*len(step)
fscore_list = [0]*len(step)
recall_list = [0]*len(step)

#for averaging stuff
average = range(35)

for avg1 in average:
#test loop
    count = 0
    print avg1
    for test_size in step:
        random.shuffle(f_pos)
        random.shuffle(f_neg)

        positive_train = f_pos[:int(n)]
        positive_test = f_pos[int(n):]

        negative_train = f_neg[:len(positive_train)]
        negative_test = f_neg[len(positive_train):len(positive_test)+len(positive_train)]

        p = predictor.Predictor(positive_train[:test_size], negative_train[:test_size], bivalues=True)


        tn = 0 #case negative, found negative
        fn = 0 #case positive, found negative
        tp = 0 #case positive, found positive
        fp = 0 #case negative, found positive

        for i in range(len(positive_test)):
            x = p.test(positive_test[i])
            pos = x[0]
            neg = x[1]
            if pos > neg:
                tp += 1
            else:
                fn += 1

        for i in range(len(negative_test)):
            x = p.test(negative_test[i])
            pos = x[0]
            neg = x[1]
            if pos < neg:
                tn += 1
            else:
                fp += 1

        tp = float(tp)
        accuracy = (tp + tn) / (len(positive_test)+len(negative_test))
        recall = tp/(tp+fn)
        precision = tp / (tp + fp)
        fscore = 2.0*precision*recall/(precision+recall)

        accuracy_list[count] += accuracy
        recall_list[count] += recall
        precision_list[count] += precision
        fscore_list[count] += fscore
        count += 1
    

for i in range(len(accuracy_list)):
    accuracy_list[i] = accuracy_list[i]/len(average)
    recall_list[i] = recall_list[i]/len(average)
    precision_list[i] = precision_list[i]/len(average)
    fscore_list[i] = fscore_list[i]/len(average)

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as pyplot
X = step
pyplot.plot(X, accuracy_list, '-')
pyplot.plot(X, recall_list, '-')
pyplot.plot(X, precision_list, '-')
pyplot.plot(X, fscore_list, '-')
legend = ['Accuracy', 'Recall', 'Precision', 'F-Score']
pyplot.legend(legend, loc='lower right')
pyplot.title('Quality Statistics (Tuples)')
pyplot.xlabel('Training Size')
pyplot.ylabel('Percentages %')
pyplot.savefig('quality_Tuples.png')
