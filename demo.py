import predictor
import random
import string

f_pos = 'data/positive_data.txt'
f_neg = 'data/negative_data.txt'


f_pos = open(f_pos).readlines()
f_neg = open(f_neg).readlines()

for i in range(len(f_pos)):
    for punct in string.punctuation:
        f_pos[i] = f_pos[i].replace(punct,"")
    f_pos[i] = f_pos[i].strip().lower()

for i in range(len(f_neg)):
    for punct in string.punctuation:
        f_neg[i] = f_neg[i].replace(punct,"")
    f_neg[i] = f_neg[i].strip().lower()

# Adding more data to the training set
more_pos = 'data/out_quotes.txt'
more_neg = 'data/fml_negative.txt'
more_pos = open(more_pos).readlines()
more_neg = open(more_neg).readlines()
#random.shuffle(more_neg)
more_neg = more_neg[:len(more_pos)]

for i in more_pos:
    for punct in string.punctuation:
        i = i.replace(punct,"")
    f_pos.append(i.strip('\r\n').lower())

for i in more_neg:
    for punct in string.punctuation:
        i = i.replace(punct,"")
    f_neg.append(i.strip('\r\n').lower())
# finish adding more data to the training set

positive_train = f_pos
negative_train = f_neg

print len(positive_train)
print len(negative_train)

p = predictor.Predictor(positive_train, negative_train)
bivalue = predictor.Predictor(positive_train, negative_train, bivalues=True)
